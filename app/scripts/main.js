$(document).ready(function(){
  $('.welcome-slider').on('init', function(event, slick, direction){
      var circle = "<svg  viewBox='0 0 26 26' class='welcome-slider__svg'><circle class='welcome-slider__circle' cx='13' cy='13' r='12' fill='transparent' stroke='#f3994b' stroke-width='2'></circle></svg>";
      $(circle).appendTo(slick.$dots.find('li'));
  });

  $('.welcome-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    fade: true
  });

  $('.special-list__slider').slick({
    dots: false,
    infinite: false,
    prevArrow: '<div class="special-list__slider-arrow special-list__slider-arrow-prev"><svg class="icon icon--left-arrow"><use xlink:href="img/svg-sprite.svg#left-arrow"></use></svg></div>',
    nextArrow: '<div class="special-list__slider-arrow special-list__slider-arrow-next"><svg class="icon icon--right-arrow"><use xlink:href="img/svg-sprite.svg#right-arrow"></use></svg></div>'
  });

  $('a[data-rel^=lightcase]').lightcase();

  $('.comments-slider').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<div class="comments-slider__arrow comments-slider__arrow--prev"><svg class="icon icon--left-arrow"><use xlink:href="img/svg-sprite.svg#left-arrow"></use></svg></div>',
    nextArrow: '<div class="comments-slider__arrow comments-slider__arrow--next"><svg class="icon icon--right-arrow"><use xlink:href="img/svg-sprite.svg#right-arrow"></use></svg></div>'
  });

  $('.phone-mask').inputmask("+7 (999) 999-99-99");

  function mapCont() {
    if($("#map").length) {
      var mapInfoPosition = $('.map__info').offset(),
          mapInfoWidth = $('.map__info').outerWidth(),
          x = window.innerWidth - (mapInfoPosition.left + mapInfoWidth);
      $('.map__cont').css('width',x);
    }
  }
  mapCont();

  $(window).resize(function(){
    mapCont();
  });

  $('.header__btn').click(function(e){
    e.preventDefault();
    $(this).toggleClass('header__btn--active');
    $('.header__cont').toggleClass('header__cont--active');
  });

  $(document).on('click',function(e){
    if(!$(e.target).closest('.header__btn,.header__cont').length) {
      $('.header__cont').removeClass('header__cont--active');
      $('.header__btn').removeClass('header__btn--active');
    }
  });
});

if (!window.Cypress) AOS.init();
